#!/usr/bin/env python3

"""Generate all meta-package files bsed on the main config file
and what is already present under the target directory
"""

import argparse
import logging
import os
import re
import shutil
import textwrap
from collections import defaultdict
from pathlib import Path

import jinja2
import ruamel.yaml
from dateutil import parser as dateparser

# configure logging
try:
    from coloredlogs import ColoredFormatter as _Formatter
except ImportError:
    _Formatter = logging.Formatter
_DEFAULT_LOG_HANDLER = logging.StreamHandler()
_DEFAULT_LOG_HANDLER.setFormatter(_Formatter(
    fmt="[%(asctime)s | %(levelname)+5s] (%(name)s): %(message)s",
))

# directories
ROOT = Path.cwd().absolute()

SELECTOR_REGEX = re.compile(
    r" # \[(.*)\](\Z|,)",
)

# supported distributions
RPM_DISTS = (
    "el7",
    "el8",
    "el9",
)
DEBIAN_DISTS = {
    "stretch": 9,
    "buster": 10,
    "bullseye": 11,
    "bookworm": 12,
    "trixie": 13,
}

yaml = ruamel.yaml.YAML()


# -- conda-build imports ------------------------

try:
    raise ModuleNotFoundError
    from conda_build.metadata import select_lines
except ModuleNotFoundError:
    sel_pat = re.compile(r'(.+?)\s*(#.*)?\[([^\[\]]+)\](?(2)[^\(\)]*)$')

    # simplified copy from conda-build 3.22.0, which is BSD-3-Clause
    def _parseNameNotFound(error):
        m = re.search('\'(.+?)\'', str(error))
        if len(m.groups()) == 1:
            return m.group(1)
        else:
            return ""

    def _eval_selector(selector_string, namespace):
        try:
            return eval(selector_string, namespace, {})
        except NameError as e:
            missing_var = _parseNameNotFound(e)
            next_string = selector_string.replace(missing_var, "False")
            return _eval_selector(next_string, namespace)

    def select_lines(data, namespace, variants_in_place):
        lines = []

        for i, line in enumerate(data.splitlines()):
            line = line.rstrip()

            trailing_quote = ""
            if line and line[-1] in ("'", '"'):
                trailing_quote = line[-1]

            if line.lstrip().startswith('#'):
                # Don't bother with comment only lines
                continue
            m = sel_pat.match(line)
            if m:
                cond = m.group(3)
                if _eval_selector(cond, namespace):
                    lines.append(m.group(1) + trailing_quote)
            else:
                lines.append(line)
        return '\n'.join(lines) + '\n'


# -- utilities ----------------------------------

# improve datetime parsing
def timestamp_constructor(loader, node):
    return dateparser.parse(node.value, ignoretz=False)


yaml.Constructor.add_constructor(
    'tag:yaml.org,2002:timestamp',
    timestamp_constructor,
)


def _platform(dist):
    if dist in RPM_DISTS:
        return "rpm"
    if dist in DEBIAN_DISTS:
        return "deb"
    return dist


def _dist_and_platform(dist):
    plat = _platform(dist)
    if dist == plat:
        return (dist,)
    return (dist, plat)


def _yaml_context(dist):
    ctx = {
        dist: True,
    }
    if dist in RPM_DISTS:
        ctx["rpm"] = True
        ctx["el"] = int(dist[2:])
    if dist in DEBIAN_DISTS:
        ctx["deb"] = DEBIAN_DISTS[dist]
    return ctx


def parse_yaml(raw, dist, logger=None):
    """Parse a YAML file for this distribution.
    """
    namespace = _yaml_context(dist)
    meta = yaml.load(select_lines(raw, namespace, False))

    # handle legacy keys
    _update_legacy_keys(meta, logger)

    return meta


def _update_legacy_keys(meta, logger=None):
    """Map legacy keys to new names.
    """
    for old, new in (
        ("desc_short", "summary"),
        ("desc_long", "description"),
        ("package_name", "name"),
    ):
        if old in meta:
            meta[new] = meta.pop(old)
            if logger:
                logger.warning(
                    f"found old key '{old}', please rename to '{new}'",
                )


def get_package_name(pkg_data, dist, default):
    """Get the desired package name from the `name` YAML attribute.

    If this is given as a string, that will be used for all distributions,
    otherwise find key for the desired ``dist`` and use it, returning the
    ``default`` if all else fails.
    """
    # get the package_name attribute
    try:
        pkg_name = pkg_data["name"]
    except KeyError:
        return default

    # if the package_name is just a name, use it
    if isinstance(pkg_name, str):
        return pkg_name

    # otherwise search for each distribution
    for key in _dist_and_platform(dist):
        try:
            return pkg_name[key]
        except KeyError:
            continue
    return default


def check_duplicate_versions(pkg_data):
    """Check for duplicate versions in the changelog for this package.

    Raises
    ------
    ValueError
        If a given ``(version, build)`` tuple is matched more than once
        in the changelog.
    """
    versions = set()
    for entry in pkg_data["changelog"][::-1]:
        vers = (entry["version"], entry["build"])
        if vers in versions:
            raise ValueError(
                f"duplicate version: {entry['version']}-{entry['build']} "
                f"(author: {entry['author']}, date: {entry['date']})",
            )
        versions.add((entry["version"], entry["build"]))


def get_dependencies(pkg_data, dist):
    """Parse the dependencies for this distribution
    """
    deps = pkg_data.get("deps") or []

    # if using a list, just return the list
    if isinstance(deps, list):
        yield from deps
        return

    # otherwise it's a map with one entry per dependencies
    for pkg, req in deps.items():

        if req is None:  # no build specifics for any build
            yield pkg
            continue

        # find if package is specified by dist (e.g. 'buster')
        # or build (e.g. 'deb') if at all
        try:
            key = [
                key for key in _dist_and_platform(dist)
                if key is not None and key in req
            ][0]
        except IndexError:  # not specified
            continue

        # key is specified, if no value just use the parent name
        raw = req[key] or pkg

        # parse a comma-separated list
        if not isinstance(raw, list):
            yield from map(str.strip, raw.split(","))
            continue

        # or a yaml list
        yield from raw


def _get_extra_headers(pkg_data, dist):
    eh = pkg_data.setdefault("extra_headers", {})
    for key in _dist_and_platform(dist):
        if key in eh:
            return eh[key]
    return {}


# -- RPM ----------------------------------------

SPEC_TEMPLATE = jinja2.Template("""
Name: {{ pkg_data['name'] }}
Version: {{ pkg_data['changelog'][0]['version'] }}
Release: {{ pkg_data['changelog'][0]['build'] }}%{?dist}
License: GPL-3.0-or-later
URL: https://git.ligo.org/packaging/metapackages
Summary: {{ pkg_data['summary'] }}
BuildArch: noarch

{% for dep in dependencies -%}
Requires: {{ dep }}
{% endfor %}

{% for line in pkg_data.get("extra_headers", {}).get("rpm", []) -%}
{{ line }}
{% endfor %}

%description
{{ pkg_data["description"] }}

%prep

%build

%install

%files

%changelog
{% for entry in pkg_data.get("changelog", []) -%}
* {{ entry["date"].strftime('%a %b %e %Y') }} {{ entry["author"] }} {{ entry["version"] }}-{{ entry["build"] }}
{% for change in entry["changes"] -%}
- {{ change.split('\n')|join('\n    ') }}
{% endfor %}
{% endfor %}
""".strip())  # noqa: E501


def rpm_create_source(pkg_data, dist, outdir):
    pkg = pkg_data["name"]

    # get dependencies
    dep_list = list(get_dependencies(pkg_data, dist))

    # if extra headers are specified, add them verbatim here
    extra_headers = _get_extra_headers(pkg_data, dist)

    # write spec file
    spec = outdir / "{}.spec".format(pkg)
    with spec.open("w") as specf:
        print(
            SPEC_TEMPLATE.render(
                pkg_data=pkg_data,
                dependencies=dep_list,
                extra_headers=extra_headers,
            ).strip(),
            file=specf,
        )


# -- debian -------------------------------------

DEBIAN_CHANGELOG_TEMPLATE = jinja2.Template("""
{% for entry in pkg_data['changelog'] -%}
{{ pkg_data["name"] }} ({{ entry['version'] }}-{{ entry['build'] }}) unstable; urgency=medium
{% for change in entry['changes'] %}
  * {{ change.replace('\n', '\n  * ') }}
{%- endfor %}

 -- {{ entry['author'] }}  {{ entry['date'].strftime('%a, %d %b %Y %H:%M:%S %z') }}

{% endfor %}
""".strip())  # noqa: E501

DEBIAN_COPYRIGHT_TEMPLATE = jinja2.Template("""
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: {{ pkg_data["name"] }}
Upstream-Contact: The LIGO Scientific Collaboration
Source: https://git.ligo.org/packaging/metapackages

Files: *
Copyright: {{ pkg_data["date"].strftime("%Y") }} LIGO Scientific Collaboration
License: GPL-2.0-or-later

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

""".strip())

DEBIAN_CONTROL_TEMPLATE = jinja2.Template("""
Section: {{ pkg_data['section'] }}
Priority: {{ pkg_data['priority'] }}
Standards-Version: 3.9.8
Package: {{ pkg_data['name'] }}
Maintainer: {{ pkg_data['maintainer'] }}
Readme: README
Changelog: changelog.Debian
Copyright: copyright
Architecture: all
{% for line in pkg_data.get("extra_headers", {}).get("deb", []) -%}
{{ line }}
{% endfor -%}
Depends:
{%- for dep in dependencies %}
 {{ dep }},
{%- endfor %}
Description: {{ pkg_data['summary'] }}
{{ pkg_data['description'] }}
""".strip())


def _debian_format(text, width=78):
    parts = text.strip().split('\n\n')
    return '\n .\n'.join(
        "\n".join(textwrap.wrap(
            p,
            width=78,
            subsequent_indent=" ",
            initial_indent=" ",
        )) for p in parts
    )


def deb_create_source(pkg_data, dist, outdir):
    _deb_control(pkg_data, dist, outdir)
    _deb_readme(pkg_data, outdir)
    _deb_changelog(pkg_data, outdir)
    _deb_copyright(pkg_data, outdir)


def _deb_readme(pkg_data, outdir):
    with (outdir / "README").open("w") as readme:
        print(pkg_data["description"], file=readme)


def _deb_changelog(pkg_data, outdir):
    with (outdir / "changelog.Debian").open("w") as readme:
        print(
            DEBIAN_CHANGELOG_TEMPLATE.render(
                pkg_data=pkg_data,
            ).strip(),
            file=readme,
        )


def _deb_copyright(pkg_data, outdir):
    with (outdir / "copyright").open("w") as readme:
        print(
            DEBIAN_COPYRIGHT_TEMPLATE.render(
                pkg_data=pkg_data,
            ).strip(),
            file=readme,
        )


def _deb_control(pkg_data, dist, outdir):
    # get dependencies
    dep_list = list(get_dependencies(pkg_data, dist))

    # if extra headers are specified, add them verbatim here
    extra_headers = _get_extra_headers(pkg_data, dist)

    # reformat long description
    pkg_data["description"] = _debian_format(
        pkg_data.get("description", pkg_data["summary"]),
    )

    # write spec file
    control = outdir / "control"
    with control.open("w") as contf:
        print(
            DEBIAN_CONTROL_TEMPLATE.render(
                pkg_data=pkg_data,
                dependencies=dep_list,
                extra_headers=extra_headers,
            ).strip(),
            file=contf,
        )


# -- conda --------------------------------------

CONDA_TEMPLATE = jinja2.Template("""
package:
  name: "{{ pkg_data['name'] }}"
  version: "{{ pkg_data['changelog'][0]['version'] }}"

build:
  number: {{ pkg_data['changelog'][0]['build'] - 1 }}
{% if noarch %}  noarch: {{ noarch }}{%- endif %}

requirements:
{%- if not noarch and 'python' in dependencies %}
  host:
    - python
{%- endif %}
  run:
{%- for dep in dependencies|sort %}
    - {{ dep }}
{%- endfor %}

test:
  commands:
{%- for cmd in pkg_data.get("test", []) %}
    - {{ cmd }}
{%- endfor %}

about:
  home: "https://git.ligo.org/packaging/metapackages"
  license: "GPL-3.0-or-later"
  license_family: "GPL"
  license_file: "LICENSE"
  summary: "{{ pkg_data['summary'] }}"
  description: |
    {{ pkg_data['description'].strip()|indent }}
""")


def conda_create_recipe(pkg_data, dist, outdir):
    # get dependencies
    dep_list = list(get_dependencies(pkg_data, dist))

    # if any packages use selectors, they can't be noarch
    if any(map(SELECTOR_REGEX.search, dep_list)):
        noarch = None
    # otherwise they will be noarch generic since they don't
    # bundle any content
    else:
        noarch = "generic"

    # write spec file
    meta = outdir / "meta.yaml"
    with meta.open("w") as specf:
        print(
            CONDA_TEMPLATE.render(
                pkg_data=pkg_data,
                dependencies=dep_list,
                noarch=noarch,
            ).strip(),
            file=specf,
        )

    # copy license file into conda directory
    shutil.copyfile(
        str(ROOT / "LICENSE"),
        str(meta.parent / "LICENSE"),
    )


# -- tests --------------------------------------

TESTS_TEMPLATE = jinja2.Template("""
#!/bin/bash

set -ex

{%- for cmd in commands %}
{{ cmd }}
{%- endfor %}
""".strip())


def write_tests(pkg_data, outdir):
    with (outdir / "test.sh").open("w") as testf:
        print(
            TESTS_TEMPLATE.render(
                commands=pkg_data.get("test") or [],
            ).strip(),
            file=testf,
        )


def render_gitlab_ci_test_pipeline(
    outdir,
    template_file=Path(".gitlab") / "ci" / "test.yml.j2",
):
    """Render the gitlab CI test child pipeline template.
    """
    metapackages = defaultdict(list)
    for meta in filter(os.path.isdir, outdir.iterdir()):
        for platform in filter(os.path.isdir, meta.iterdir()):
            metapackages[platform.name].append(meta.name)
    metapackages["all"] = set(
        meta for platform in metapackages
        for meta in metapackages[platform]
    )

    with open(template_file, "r") as tmplt:
        template = jinja2.Template(tmplt.read())
    out = template.render({"metapackages": metapackages})
    with open(template_file.parent / template_file.stem, "w") as file:
        print(out.strip(), file=file)


# -- main ---------------------------------------

BUILDERS = {
    "conda": conda_create_recipe,
}
BUILDERS.update({dist: rpm_create_source for dist in RPM_DISTS})
BUILDERS.update({dist: deb_create_source for dist in DEBIAN_DISTS})


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-m",
        "--metapackage-directory",
        default=ROOT / "meta",
        type=Path,
        help="directory containing metapackage YAML files",
    )
    parser.add_argument(
        "-o",
        "--output-directory",
        default=ROOT / "stage",
        type=Path,
        help="output directory for package files",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="display debugging output",
    )
    return parser


def create_logger(name, debug=False):
    logger = logging.getLogger(name)
    if not logger.level:
        logger.setLevel(logging.DEBUG if debug else logging.INFO)
    if not logger.hasHandlers():
        logger.addHandler(_DEFAULT_LOG_HANDLER)
    return logger


def process_metapackage(meta_file, outdir, debug=False):
    logger = create_logger(meta_file.stem, debug=debug)

    logger.info(f"processing {meta_file.name}")

    # read the raw metapackage file (but don't parse it)
    pkg = meta_file.stem
    with meta_file.open("r") as metaf:
        rawmeta = metaf.read()

    # build for each type
    for dist, build_func in BUILDERS.items():
        # parse the YAML for this dist
        meta = parse_yaml(rawmeta, dist, logger)

        # skip for this platform
        if any(
            x in meta.get("skip", [])
            for x in _dist_and_platform(dist)
        ):
            logger.debug(f"skipping {dist}")
            continue

        # a few modifications need to be done (convenience)
        # parse date/times from changelog and sort them with newest first
        meta["changelog"].sort(key=lambda x: x["date"], reverse=True)

        # set default build number
        [e.setdefault("build", 1) for e in meta["changelog"]]

        # check for duplicate versions
        check_duplicate_versions(meta)

        # add long description unless given
        meta.setdefault("description", meta["summary"])
        # add date of most recent version
        meta["date"] = meta["changelog"][0]["date"]

        # basic tests
        for key in (
            "changelog",
            "summary",
            "description",
            "maintainer",
            "priority",
            "section",
        ):
            assert key in meta, f"{meta_file} requires key '{key}'"

        # copy metadata so we can modify it as necessary per dist
        pkg_data = meta.copy()

        # set the name of this package
        # (which may be specific for each distribution)
        pkg_data["name"] = get_package_name(meta, dist, pkg)

        # once we get here, check if we need to work at all on this one
        # for that, check its version file and compare to latest one from
        # changelog
        versionfile = (outdir / pkg_data["name"] / "version")
        try:
            with versionfile.open("r") as verf:
                version = verf.read().strip()
        except FileNotFoundError:
            pass
        else:
            if version == str(pkg_data['changelog'][0]['version']).strip():
                logger.debug(
                    f"up-to-date for {dist}, skipping",
                )
                continue

        logger.debug(f"processing {dist}")

        distdir = outdir / pkg_data["name"] / dist
        distdir.mkdir(parents=True, exist_ok=True)

        build_func(pkg_data, dist, distdir)
        write_tests(pkg_data, distdir)

    # all done, then update version file
    with versionfile.open("w") as verf:
        print(str(meta["changelog"][0]["version"]), file=verf)


def main(args=None):
    parser = create_parser()
    args = parser.parse_args()

    for meta_file in args.metapackage_directory.glob("*.yml"):
        process_metapackage(
            meta_file,
            args.output_directory,
            debug=args.verbose,
        )

    if os.getenv("GITLAB_CI"):
        render_gitlab_ci_test_pipeline(args.output_directory)


if __name__ == "__main__":
    main()
