<!--

Thank you for your interest in configuring a new IGWN software metapackage.

Please complete the following template as best you can to allow the
IGWN Computing Team to evaluate and act on your request.

If you can have questions about what any of this means, please contact

compsoft@ligo.org

-->

#### New metapackage request

**Name**: `igwn-<FIXME>`

**Platforms**:

<!-- please select one (or more) of the following platforms -->

- [ ] Conda (conda-forge)
- [ ] Debian 11
- [ ] Rocky Linux 8

**Purpose**:

<!-- Describe the use-case for this metapackage here. -->

**Contents**:

<!-- please list, if you can, some or all of the packages that
should be included in the metapackage. The package names don't have
to be exact (package names can be fiddly to track across distributions),
just a best guess is fine -->

- 
- 
