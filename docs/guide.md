---
title: User guide
---

# IGWN Metapackages User Guide {: #guide }

## How to install the metapackages {: #install }

The metapackages are included in the software distributions maintained
by the IGWN Computing and Software Group.
See the supported 'Repositories' linked from the
[_IGWN Software Overview_](https://computing.docs.ligo.org/guide/software/)
on the [IGWN Computing Guide](https://computing.docs.ligo.org/guide/) for full
details of how to configure your package manager to discover the metapackages.

!!! note "CONTRIBUTING.md"
    The following is a verbatim copy of
    [`CONTIBUTING.md`](https://git.ligo.org/packaging/metapackages/-/blob/main/CONTRIBUTING.md)
    from the IGWN Metapackages repository.

{% include "../CONTRIBUTING.md" start="<!-- INCLUDE -->"%}
